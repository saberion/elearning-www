

$(window).resize(function() {
    //equal heights
    $('.height-main').each(function(){
        var prntheight = $(this).height();
        $(this).parent().children('.height-child').height(prntheight);
    });


});

$(window).resize();

$('.same-height-block').matchHeight({
    byRow: false,
});

$('.same-height').matchHeight({
    byRow: false,
});

$('.same-height-row').matchHeight({
    byRow: true,
});

$('a.like').click(function(){
    $(this).toggleClass('active');
});



$( '[data-fancybox="gallery"]' ).fancybox({
    caption : function( instance, item ) {
        var caption = $(this).data('caption') || '';
         var capttxt =  caption.caption;
        var capttime =  caption.time;
        caption = '<div class="caption-wrap"><div class="caption-txt">'+capttxt+'</div><div class="caption-time">'+capttime+'</div></div>';
        return caption;
    }
});


$('.search-btn').click(function(){
    $('.searchblock-mobile').slideToggle(200);
});





var path = location.href.substring(0, location.href.lastIndexOf("/")+1);
$('.nav-wraper ul a').each(function() {
    if (this.href === path) {
        $(this).addClass('active');
    }
});



$(".scrollbar-inner").mCustomScrollbar();
if ($(window).width() < 1023) {
    $('.scrollbar-inner').mCustomScrollbar("destroy")
}


$("#filter-course").on('change', function() {
    if ($(this).val() == 'all'){
        $('.element-block-wrap').show();
    } else {
        var selectval  = $(this).val();
        $('.element-block-wrap').hide();

        $('.element-block-wrap[data-value="'+selectval+'"]').show();
    }
});